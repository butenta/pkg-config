package config

// Vat structure
type Vat struct {
	Status   string
	Value    float64
	ProcMode string
}
