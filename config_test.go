package config

// Function LoadMain read default values from yaml config file and initialize
// App, Butent and Database configurations
// func ExampleLoadMain() {
// 	cfg, err := config.LoadMain()
// }

// Function LoadCustom load configuration data from custom section and put values to
// custom structure
// func ExampleLoadCustom() {
// 	cust := &CustomStruct{}
//
// 	err := config.LoadCustom("custom", &cust)
// }
